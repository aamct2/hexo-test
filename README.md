### Hexo Test

This is a test repository for using the [Hexo][hexo] software for generating a static site.

### License

This repository is licensed under the [MIT license][license].

<!-- Links -->

[license]: ./LICENSE
[hexo]: https://hexo.io/
