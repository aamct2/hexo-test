HEXO := $(shell which hexo)
NPM := $(shell which npm)
PROSELINT := $(shell which proselint)

# - Build & Set Up

# Builds the blog
build:
	$(HEXO) generate
.PHONY: build

# Deploys the blog
deploy:
	$(HEXO) deploy
.PHONY: deploy

# Sets up the workspace (typically used after a fresh clone)
# - Installs dependencies, builds the site
setup: dependencies
	$(MAKE) build
.PHONY: setup

# - Lint

# Lints the source files
lint:
	$(PROSELINT) ./source/
.PHONY: lint

# - Run

# Runs the Hexo server for local testing
run:
	$(HEXO) server
.PHONY: run

# - Blog

post:
ifndef title
	$(error "Missing `title` argument.")
endif
	$(HEXO) new "$(title)"
.PHONY: post

# - Dependencies

# Installs the dependencies
dependencies:
	@$(MAKE) install-node

install-node:
ifndef NPM
	$(error "Couldn't find Node installed.")
endif
	$(NPM) i -g hexo-cli
	$(NPM) install
